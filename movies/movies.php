<?php

/**
 * Plugin name: Movies
 */

namespace Movies;

use Widget;

class Movie
{
    public static function register()
    {
        $instance = new self;

        add_action('init', [$instance, 'addMenu']);
        add_action( 'add_meta_boxes', array( $instance, 'load_meta_box') );
        add_shortcode( 'sc_get_posts_via_rest', [$instance, 'get_posts_via_rest'] );
        add_shortcode( 'sc_get_posts_via_query', [$instance, 'get_posts_via_query'] );
        add_shortcode( 'sc_get_number_of_movies', [$instance, 'get_number_of_movies'] );

    }

    public function addMenu()
    {
        register_post_type(
            'movies',
            array(
                'labels' => array(
                    'name' => __('Movies'),
                    'singular_name' => __('Movie')
                ),
                'public' => true,
                'show_in_rest' => true,
                'rest_base' => 'result',
                'rest_controller_class' => 'WP_REST_Posts_Controller',
                'supports' => array('title', 'editor', 'thumbnail'),
                'has_archive' => true,
                'rewrite'   => array('slug' => 'my-home-movies'),
                'menu_position' => 5,
                'menu_icon' => 'dashicons-admin-media',
                'taxonomies' => array('movies', 'post_tag', 'category')
            )
        );
    }

    public function load_meta_box(){
        add_meta_box( 'cmb_meta', __( 'Extra Info', 'cmb-textdomain' ), array( $this, 'addExtraField'), 'movies' ); // <== HERE
    }
    public function addExtraField($post){
        $values = get_post_custom($post->ID);

        $extra_info = isset($values['extra_info']) ? $values['extra_info'] : '';   ?>

        <textarea rows="4" style="width: 100%;" name="extra_info" id="extra_info"><?php echo $extra_info[0]; ?></textarea>
        </br> <?php
    }

    function get_posts_via_rest() {
        $allPosts = '';
        $response = wp_remote_get( "http://localhost:8001/wp-json/wp/v2/movies?per_page=10" );
        if ( is_wp_error( $response ) ) return;
        $posts = json_decode( wp_remote_retrieve_body( $response ) );

        if ( empty( $posts ) )  return;

        if ( ! empty( $posts ) ) {
            foreach ( $posts as $post ) $allPosts .= '<a href="' . esc_url( $post->link ) . '" target=\"_blank\">' . esc_html( $post->title->rendered ) . '</a>  ' . '<br />';

            return $allPosts;
        }
    }

    function get_posts_via_query() {

        $allPosts = '';
        $loop = new \WP_Query(array( 'post_type' => 'movies'));

        while ( $loop->have_posts() ) : $loop->the_post();
            $allPosts .= '<a href="' . get_permalink() . '" target=\"_blank\">' . get_the_title() . '</a>  ' . '<br />';
        endwhile;

        wp_reset_postdata();

        return $allPosts;
    }

    function get_number_of_movies() {

        $movies = new \WP_Query(array( 'post_type' => 'movies'));

        return $movies->post_count;
    }
}

Movie::register();
